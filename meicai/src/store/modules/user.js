/**
 * vuex module: user
 */

import Cookies from 'js-cookie';

const user = {
  state: {
    id: Cookies.get('id'),
    name: Cookies.get('nickname'),
    token: Cookies.get('token'),
    status: '',
  },

  mutations: {
    setId (state, id) {
      state.id = id;
    },
    setName (state, name) {
      state.name = name;
    },
    setToken (state, token) {
      state.token = token;
    },
    setStatus (state, status) {
      state.status = status;
    }
  },

  actions: {

    /**
     * 登录
     * @param commit
     * @param userInfo
     */
    login({commit}, userInfo) {
      Cookies.set('id', userInfo.id);
      Cookies.set('nickname', userInfo.nickname);
      Cookies.set('token', userInfo.token);
      commit('setId', userInfo.id);
      commit('setName', userInfo.nickname);
      commit('setToken', userInfo.token);
    },

    /**
     * 登出
     * TODO 前端简单登出
     * @param commit
     * @param state
     */
    logout({commit, state}) {
      commit('setId', '');
      commit('setName', '');
      commit('setToken', '');
      Cookies.remove('id');
      Cookies.remove('nickname');
      Cookies.remove('token');
    },

  // 获取用户信息
/*    GetInfo({commit, state}) {
    return new Promise((resolve, reject) => {
      getInfo(state.token).then(response => {
        const data = response.data;
        commit('SET_ROLES', data.role);
        commit('SET_NAME', data.name);
        commit('SET_AVATAR', data.avatar);
        commit('SET_UID', data.uid);
        commit('SET_INTRODUCTION', data.introduction);
        resolve(response);
      }).catch(error => {
        reject(error);
      });
    });
  },*/


    // 登出
/*    LogOut({commit, state}) {
      return new Promise((resolve, reject) => {
        logout(state.token).then(() => {
          commit('SET_TOKEN', '');
          commit('SET_ROLES', []);
          Cookies.remove('Admin-Token');
          resolve();
        }).catch(error => {
          reject(error);
        });
      });
    },*/

    // 前端 登出
/*    FedLogOut({commit}) {
      return new Promise(resolve => {
        commit('SET_TOKEN', '');
        Cookies.remove('Admin-Token');
        alert("has logout");
        resolve();
      });
    },*/

    // 动态修改权限
/*    ChangeRole({commit}, role) {
      return new Promise(resolve => {
        commit('SET_ROLES', [role]);
        commit('SET_TOKEN', role);
        Cookies.set('Admin-Token', role);
        resolve();
      })
    }*/

  }
};

export default user;

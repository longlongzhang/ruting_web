import axios from 'http'

/**
 * get 请求
 */
export function get(url, params) {
  // axios.defaults.headers.common['Authorization'] = store.state.user.token;
  return new Promise((resolve, reject) => {
    axios.get(url, params)
      .then(response => {
        resolve(response.data);
      }, err => {
        reject(err);
      })
      .catch((error) => {
        reject(error)
      })
  })
}

/**
 * post 请求
 */
export function post(url, params) {
  // axios.defaults.headers.common['Authorization'] = store.state.user.token;
  return new Promise((resolve, reject) => {
    axios.post(url, params)
      .then(response => {
        resolve(response.data);
      }, err => {
        reject(err);
      })
      .catch((error) => {
        reject(error)
      })
  })
}

export default {
  /**
   * 登录
   */
  login: 'user/login',

  /**
   * 商圈list
   */
  getStationRegionList: 'basic/stationRegion/list',

  /**
   * 客户订单列表
   */
  getBasicOrderList: 'basic/order/list',

  /**
   * 仓库列表
   */
  getWearhouseList: "basic/wearhouse/list?data=&page=1&pageSize=10",

  /**
   * 商户列表
   */
  getCompanyList: 'basic/company/list',

  /**
   * 路由列表
   */
  getRouteList: 'output/route/list',

  /**
   * 路由详情列表
   */
  getRouteDetailList: 'output/route/detail/list',

  /**
   * carCapacity 列表
   */
  getCarCapacityList: 'basic/car/capacity/list',

  /**
   * carType
   */
  getCarTypeList: 'basic/car/type/list',

  getOrderList: 'output/order/list',

  /**
   * unRouteOrderList
   * @param params
   */
  getUnRouteOrderList: 'output/order/unrouted',

  getPlanSummaryList(params) {
    return get('output/summary', params);
  },

  lineRouteDetail: 'line/route/detail',

  /**
   * 商圈id
   */
  stationRegionIdList: 'basic/stationRegion/list',


  /**
   *Route  summary 列表
   */
  getRouteSummaryList: 'line/route/summary',

  /**
   * 未路由订单
   */
  getUnroutedOrderList:'line/order/unrouted',

}

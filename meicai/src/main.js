import Vue from 'vue'
import router from './router'
import http from './api/http'
import store from './store/index'
import App from './App.vue' // 主组件
import ElementUI from 'element-ui' // 引入并使用 element-ui
import 'element-ui/lib/theme-default/index.css'
import bootstrap from 'bootstrap' // 引入 bootstrap
import 'bootstrap/dist/css/bootstrap.min.css'
import AMap from 'AMap'; // 引入并使用高德地图

import echarts from 'echarts';
import 'echarts/lib/chart/map';
import 'echarts/map/js/china.js';

import '../static/css/base.css'
import '../static/css/reset.css'

Vue.use(ElementUI);
Vue.use(bootstrap);
Vue.use(AMap);

Vue.config.debug = true;          // 开启 debug
Vue.config.productionTip = false;

Vue.prototype.$axios = http;  // 全局引用 axios

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),

  created() {
  },

  methods: {
  }
});

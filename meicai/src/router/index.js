import Vue from 'vue'
import Router from 'vue-router'
//登录
import Login from '../components/login/Login.vue'
// 主页
import Home from '../components/Home.vue'
//全局概览
//基础数据  1.0
import BasicDataHome from '../components/basicData/BasicDataHome.vue'
import BasicOrder from '../components/basicData/Order.vue'
import Wearhouse from '../components/basicData/Wearhouse.vue'
import Company from '../components/basicData/Company.vue'
import CarCapacity from '../components/basicData/CarCapacity.vue'
import CarType from '../components/basicData/CarType.vue'
// import UnRouteOrder from '../components/output/UnRouteOrders.vue'
// import PlanSummary from '@/components/output/PlanSummary'
// 基础数据 2.0
// import Warehouse from '../components/basicData/Warehouse.vue'
// import Customer from '../components/basicData/Customer.vue'
// import Transport from '../components/basicData/Transport.vue'
//评估优化  1.0
//评估优化  2.0
import OptimizationHome from '../components/optimization/OptimizationHome.vue'
import Index from '../components/optimization/Index.vue'
import Finance from '../components/optimization/Finance.vue'
import CustomerService from '../components/optimization/CustomerService.vue'
import LineOptimization from '../components/optimization/LineOptimization.vue'

import Error404 from '@/components/views/error/404'
//echarts
import AreaGraphsForCurve from '../components/chart/AreaGraphsForCurve.vue'
import Curve from '../components/chart/Curve.vue'
import ChinaMap from '../components/chart/ChinaMap.vue'

import store from '../store/index'


Vue.use(Router);


const router = new Router({
  // mode: 'history',  // route mode，不设置默认 hash 模式，该模式下路径以 # 开头
  linkActiveClass: 'active',
  routes: [
    {path: '/chinaMap', name: 'ChinaMap', component: ChinaMap},
    {path: '/areaGraphsForCurve', name: 'AreaGraphsForCurve', component: AreaGraphsForCurve},
    {path: '/curve', name: 'Curve', component: Curve},
    {path: '/login', name: 'Login', component: Login},
    {
      path: '/', name: 'Home', component: Home, redirect: 'output/index', meta: {requireAuth: false},
      children: [
        // {path: 'overview', name: 'Overview', component: Overview, meta: {requireAuth: false}},
        {
          path: 'basic',
          name: 'BasicDataHome',
          redirect: 'basic/company',
          component: BasicDataHome,
          meta: {requireAuth: false},
          children: [
            {path: 'order', name: 'BasicOrder', component: BasicOrder, meta: {requireAuth: false}},
            {path: 'wearhouse', name: 'Wearhouse', component: Wearhouse, meta: {requireAuth: false}},
            {path: 'company', name: 'Company', component: Company, meta: {requireAuth: false}},
            {path: 'carCapacity', name: 'CarCapacity', component: CarCapacity, meta: {requireAuth: false}},
            {path: 'carType', name: 'CarType', component: CarType, meta: {requireAuth: false}},
          ]
        },
        {
          path: 'output',
          name: 'OptimizationHome',
          redirect: 'output/index',
          component: OptimizationHome,
          meta: {requireAuth: false},
          children: [
            {path: 'index', name: 'Index', component: Index, meta: {requireAuth: false}},
            {path: 'finance', name: 'Finance', component: Finance, meta: {requireAuth: false}},
            {path: 'customer', name: 'CustomerService', component: CustomerService, meta: {requireAuth: false}},
            {path: 'line', name: 'LineOptimization', component: LineOptimization, meta: {requireAuth: false}},
          ]
        },

        // {path: 'output/index', name: 'OptimizationIndex', component: OptimizationIndex, meta: {requireAuth: false}},
        // {path: 'output/finance', name: 'FinanceIndex', component: FinanceIndex, meta: {requireAuth: false}},
        // {path: 'output/customer', name: 'CustomerService', component: CustomerService, meta: {requireAuth: false}},
        // {path: 'output/line', name: 'LineOptimization', component: LineOptimization, meta: {requireAuth: false}},

        // {path: 'basic/company', name: 'Company', component: Company, meta: {requireAuth: true}},
        // {path: 'basic/wearhouse', name: 'Wearhouse', component: Wearhouse, meta: {requireAuth: true}},
        // {path: 'basic/car/capacity', name: 'CarCapacity', component: CarCapacity, meta: {requireAuth: true}},
        // {path: 'basic/car/type', name: 'CarType', component: CarType, meta: {requireAuth: true}},
        // {path: 'basic/order', name: 'BasicOrder', component: BasicOrder, meta: {requireAuth: true}},
        // {path: 'output/route', name: 'Route', component: Route, meta: {requireAuth: true}},
        // {path: 'output/order', name: 'Order', component: Order, meta: {requireAuth: true}},
        // {path: 'output/order/unrouted', name: 'UnRouteOrder', component: UnRouteOrder, meta: {requireAuth: true}},
        // {path: 'output/plansummary', name: 'PlanSummary', component: PlanSummary, meta: {requireAuth: true}},
        // {path: 'file/upload', name: 'FileUpload', component: FileUpload, meta: {requireAuth: true}}
      ]
    },
    {path: "*", component: Error404},
  ]
});

// 路由拦截
router.beforeEach((to, from, next) => {
  if (to.meta.requireAuth) {  // 判断该路由是否需要登录权限
    if (store.state.user.token) {  // 通过vuex state获取当前的token是否存在
      next();
    }
    else {
      next({
        path: '/login'
        // query: {redirect: to.fullPath}  // 将跳转的路由path作为参数，登录成功后跳转到该路由
      })
    }
  } else {
    next();
  }
});

// to: Route : 即将要进入的目标 [路由对象]
// from: Route : 当前导航正要离开的路由
// next: Function : 一定要调用该方法来 resolve 这个钩子。执行效果依赖 next
router.afterEach((to, from, next) => {
})

export default router;

// 日期月份/天的显示，如果是1位数，则在前面加上'0'
export  function getFormatDate(arg) {
  if (arg == undefined || arg == '') {
    return '';
  }

  var re = arg + '';
  if (re.length < 2) {
    re = '0' + re;
  }

  return re;
}


export default {
  addDate(date){
    var date = new Date(date);
    date.setDate(date.getDate() + 1);
    var month = date.getMonth() + 1;
    var day = date.getDate();
    return date.getFullYear() + '-' + getFormatDate(month) + '-' + getFormatDate(day);
  }
}
